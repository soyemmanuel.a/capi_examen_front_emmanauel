import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.css']
})
export class TablaUsuariosComponent implements OnInit {

  constructor() { }
  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      ajax: 'http://pruebas.test/api/usuarios',
      columns: [{
        title: 'Nombre de usuario',
        data: 'name'
      },
      {
        title: 'Fecha de nacimiento',
        data: 'fecha_nacimiento'
      },
      {
        title: 'Edad',
        data: 'edad'
      },
      {
        title: 'Dirección',
        data: "domicilio" +" "+"numero_exterior"+" "+"colonia"+" "+"cp"+"  "+"ciudad "
      }
    ]
    };
  }

}
