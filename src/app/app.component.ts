import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'prueba';

  dtOptions: DataTables.Settings = {};
  ngOnInit(): void {
    this.dtOptions = {
      ajax: 'http://pruebas.test/api/usuarios',
      columns: [{
        title: 'Nombre de usuario',
        data: 'name'
      },
      {
        title: 'Fecha de nacimiento',
        data: 'fecha_nacimiento'
      },
      {
        title: 'Edad',
        data: 'edad'
      },
      {
        title: 'Dirección',
        data: "domicilio" +" "+"numero_exterior"+" "+"colonia"+" "+"cp"+"  "+"ciudad "
      }
    ]
    };
  }

}
